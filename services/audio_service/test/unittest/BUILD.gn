# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/test.gni")
import("../../../../config.gni")

module_output_path = "multimedia_audio_framework/audio_balance"

config("module_private_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "../../common/include",
    "../../client/include",
    "../../server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../../../../../foundation/systemabilitymgr/samgr/interfaces/innerkits/samgr_proxy/include",
  ]
}

ohos_unittest("audio_endpoint_separate_unit_test") {
  testonly = true
  module_out_path = module_output_path
  sources = [ "audio_endpoint_separate_unit_test.cpp" ]

  cflags = [ "-fno-access-control" ]

  include_dirs = [
    "../../server/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink/remote_fast",
    "../../../../services/audio_service/server/include",
    "../../../../services/audio_service/server/src",
    "../../../../frameworks/native/audioschedule/include",
    "../../../../frameworks/native/hdiadapter/source/fast",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../frameworks/native/playbackcapturer/include",
  ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service",
  ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libaudio_proxy_4.0",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("audio_balance_unit_test") {
  testonly = true
  module_out_path = module_output_path
  cflags = [ "-fno-access-control" ]

  include_dirs = [
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/source/common",
    "../../../../services/audio_service/client/src",
    "../../../../services/audio_service/server/include",
    "../../../../frameworks/native/playbackcapturer/include",
  ]
  sources = [
    "audio_balance_unit_test.cpp",
    "audio_service_common_unit_test.cpp",
    "audio_service_unit_test.cpp",
  ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioeffect:audio_effect",
    "../../../../frameworks/native/audioschedule:audio_schedule",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_service:audio_process_service",
    "../../../../services/audio_service:audio_service",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
  ]

  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("audio_direct_sink_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../interfaces/inner_api/native/audiocommon/include",
  ]

  cflags = [ "-DDEBUG_DIRECT_USE_HDI" ]

  sources = [ "audio_direct_sink_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:audio_renderer_sink",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("none_mix_engine_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  cflags = [ "-DDEBUG_DIRECT_USE_HDI" ]

  sources = [ "none_mix_engine_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("audio_service_unit_test") {
  module_out_path = module_output_path

  configs = [ ":module_private_config" ]

  include_dirs = [
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../interfaces/inner_api/native/audiomanager/include",
    "../../../../frameworks/native/audioinnercall/include",
    "../../../../frameworks/native/audiopolicy/include",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/source/common",
  ]

  sources = [
    "./client/audio_stream_manager_unit_test.cpp",
    "./client/audio_system_manager_unit_test.cpp",
    "./client/fast_audio_stream_unit_test.cpp",
    "./common/volume_tools_unit_test.cpp",
    "./server/audio_server_unit_test.cpp",
  ]

  deps = [
    "../../../../frameworks/native/audioeffect:audio_effect",
    "../../../../frameworks/native/audioschedule:audio_schedule",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../services/audio_service:audio_client",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_service",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "access_token:libaccesstoken_sdk",
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
  ]
}

ohos_unittest("pa_renderer_stream_impl_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
  ]
  sources = [ "pa_renderer_stream_impl_unit_test.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("pa_renderer_stream_impl_unit_test_p2") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../../../services/audio_service/server/include",
  ]
  sources = [ "pa_renderer_stream_impl_unit_test_p2.cpp" ]

  configs = [ ":module_private_config" ]
  cflags = [ "-fno-access-control" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("ipc_stream_in_server_unit_test") {
  module_out_path = module_output_path

  install_enable = false

  include_dirs = [
    "../../../../frameworks/native/audioutils/include",
    "../../../../frameworks/native/hdiadapter/common/include",
    "../../../../frameworks/native/hdiadapter/sink",
    "../../../../frameworks/native/hdiadapter/sink/primary",
    "../../../../frameworks/native/hdiadapter/sink/common",
    "../../../../frameworks/native/hdiadapter/sink/fast",
    "../../../../interfaces/inner_api/native/audiocommon/include",
    "../../../../services/audio_service/common/include",
    "../../../../services/audio_service/server/include",
  ]

  sources = [ "ipc_stream_in_server_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../../frameworks/native/hdiadapter/sink:fast_audio_renderer_sink",
    "../../../../services/audio_service:audio_common",
    "../../../../services/audio_service:audio_process_service",
  ]

  cflags = [ "-fno-access-control" ]

  external_deps = [
    "c_utils:utils",
    "googletest:gmock",
    "hilog:libhilog",
    "pulseaudio:pulse",
  ]
}

ohos_unittest("volume_tools_unit_test") {
  testonly = true
  module_out_path = module_output_path
  include_dirs = [
    "../../../../frameworks/native/audiocapturer/include",
    "../../interfaces/inner_api/native/audiocommon/include",
  ]
  sources = [ "volume_tools_unit_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/native/audiocapturer:audio_capturer",
    "../../../../frameworks/native/audioutils:audio_utils",
    "../../../audio_service:audio_client",
    "../../../audio_service:audio_common",
    "../../../audio_service:audio_process_service",
  ]
  external_deps = [
    "c_utils:utils",
    "googletest:gtest",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "ipc:ipc_single",
    "pulseaudio:pulse",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}
